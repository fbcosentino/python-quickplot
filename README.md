Quick Plot script to plot a series of X, Y coordinate series

Pairs are separated by newline ("\n") and values are separated
by commas (","). You can edit this file and change them.

# Installation #

pip install numpy

pip install matplotlib

# Usage #

python quickplot.py <your file.txt>