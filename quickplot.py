"""
Quick Plot script to plot a series of X, Y coordinate series

Pairs are separated by newline ("\n") and values are separated
by commas (","). You can edit this file and change them.

:Author:
    Fernando Cosentino

"""

# Should first line be considered labels?
# True, False, or 'auto' to autodetect from topleft value
USE_LABELS = 'auto'

# Separates lines:
PAIR_SEPARATOR = "\n"

# Separates X and Y values from eachother
VALUE_SEPARATOR = ","

# Format 
#     Common formats:
#     'o-'    -> Markers (dots) and lines
#     '-'     -> Lines only
#     'o'     -> Markers only
FORMAT = 'o-'

# Line width if your format uses lines
LINE_WIDTH = 2.0

# Marker (dots) size if your format uses markers
MARKER_SIZE = 5.0

# Color for moth markers and lines
#     Examples: 'blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black', 'white'
#     You can also use HEX format: '#EE55FF'
PLOT_COLOR = [
    'blue',
    'green',
    '#990000',
    '#009999',
    '#990099',
    '#999900',
    'black',
    '#999999'
]


##=========================================================================##
# Don't touch below this line

# (Yes, that sentence was common in source codes from the 90'es)

import numpy as np
import matplotlib.pyplot as plt

import sys

if len(sys.argv) < 2:
    print "\nUsage:\n    python plot.py filename.txt\n\n"
    quit()
else:
    filename = sys.argv[1]

with open(filename) as file:
    content = file.read()
    
lines = content.split(PAIR_SEPARATOR)

# First sanitise blank lines:
# Deleting, therefore loop must be backwards
i = len(lines)
while i > 0:
    i -= 1
    if lines[i].strip() == '':
        del lines[i]

values = {}
i = 0
val_size=0

# If auto labels, try to autodetect and switch to boolean
if USE_LABELS == 'auto':
    try:
        topleft = lines[0].split(VALUE_SEPARATOR)[0]
        if not topleft.replace('.','',1).isdigit():
            USE_LABELS = True
        else:
            USE_LABELS = False
    except:
        USE_LABELS = False

if USE_LABELS is True:
    label_line = lines[0]
    del lines[0]
    labels = label_line.split(VALUE_SEPARATOR)
    labels_y = labels[1:]
else:
    labels = None
    labels_y = None

for eline in lines:
    eline = eline.strip()

    if len(eline) > 0:
        val_list = eline.split(VALUE_SEPARATOR)
        
        vlen = len(val_list)
        
        # If only X coordinate, this is pointless
        if vlen < 2:
            continue
            
        if vlen > val_size:
            val_size = vlen
            
        for val in val_list:
            values[i] = val_list

        i += 1

# === Now sanitise the values

# What if we have only one column?
if val_size < 2:
    # This is Y-only. Adds index as X
    for i in values:
        values[i].insert(0, i)
        

from pprint import pprint

axes = []
for i in xrange(1,val_size):
    axes.append([ [], [] ])
# axes is now a 3 dimensional list
# each inner list will hold 2 lists (X and Y)
# So this is a list of lists of lists
# In other words:
#   axes[0] is the first data series,
#   axes[0][0] is the X list,
#   axes[0][1] is the Y list,
#   axes[0][1][0] is the first y record

# Reorganise in the correct format for plot
for i in values:
    val_list = values[i]
    val_x = float(val_list[0].strip())
    
    for axis in xrange(1,val_size):
        # Get Y for this data series
        if axis < len(val_list):
            val_y = val_list[axis].strip()
            if val_y == '':
                val_y = None
            else:
                val_y = float(val_y)
        else:
            val_y = None
        
        if val_y is not None:
            axes[axis-1][0].append(val_x) # axes[axis][0] is the X list
            axes[axis-1][1].append(val_y) # axes[axis][1] is the Y list
    
legend = []
for axis in xrange(0,val_size-1):
    if USE_LABELS is True:
        axis_label = labels[axis+1] # first label is X
    else:
        axis_label = "Series "+str(axis+1)
    legend.append(axis_label)
    
    plt.plot(axes[axis][0], axes[axis][1], FORMAT, color=PLOT_COLOR[axis % 8], linewidth=LINE_WIDTH, markersize=MARKER_SIZE)

if len(legend) == 1:
    if USE_LABELS is True:
        plt.title(legend[0])
    else:
        plt.title(filename)
else:
    plt.legend(legend)
    plt.title(filename)
    
plt.show()